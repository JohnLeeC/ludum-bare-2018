﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public float fps = 100;


    public Vector3 screenWrapOffset, blockOffset;

    public GameManager otherManager;
    public Transform bulletHolder, corpseHolder;
    public FileManager fileUI;

    public Text playerFPScounter, otherFPScounter, startCountdown, winText;
    public GameObject localPlayer, otherPlayer, pregameUI, endLoseUI, endWinUI, screenWrap, walls;

    public GameObject icyBlockHolder, bouncyBlockHolder;
    public List<BulletMovement> bulletList = new List<BulletMovement>();
    public GameObject[] queuedItemsInUI;
    public List<string> abilities = new List<string>();

    private Vector3 screenWrapOrigional, icyBlockOrigin, bouncyBlockOrigin;
    private Dictionary<string, int> files = new Dictionary<string, int>();
    Dictionary<string, float> fpsCostPerFile = new Dictionary<string, float>();
    Dictionary<string, GameObject> UIItems = new Dictionary<string, GameObject>();
    private bool gameOver, starting = false;


    private float timer = 0, timeConsumed = 0, gameStartTime;


    // Use this for initialization
    private void Awake()
    {
        bouncyBlockOrigin = bouncyBlockHolder.transform.position;
        icyBlockOrigin = icyBlockHolder.transform.position;
        screenWrapOrigional = screenWrap.transform.position;
        otherPlayer.GetComponent<FollowWithOffset>().StartFollow(otherManager.localPlayer.transform);
        FillDictionary();
        FillCostDict();
        FillQueueDict();
        //StartCoroutine("GameLoop");

    }
    private void Start()
    {
        updateFiles();
    }

    private void FillDictionary()
    {
        files.Add("fps", 1);
        files.Add("screenWrap", 1);
        files.Add("icyBlock", 1);
        files.Add("bouncyBlock", 1);
        
    }
    void FillCostDict()
    {
        fpsCostPerFile.Add("fps", 10);
        fpsCostPerFile.Add("screenWrap", 10);
        fpsCostPerFile.Add("icyBlock", 20);
        fpsCostPerFile.Add("bouncyBlock", 25);
    }
    void FillQueueDict()
    {
        UIItems.Add("fps", queuedItemsInUI[0]);
        UIItems.Add("screenWrap", queuedItemsInUI[1]);
        UIItems.Add("icyBlock", queuedItemsInUI[2]);
        UIItems.Add("bouncyBlock", queuedItemsInUI[3]);
    }
    void Update()
    {
        if (!fileUI.finished || !otherManager.fileUI.finished)
        {
            starting = false;
            localPlayer.GetComponent<PlayerMovement>().enabled = false;
            pregameUI.SetActive(true);
        }
        else
        {
            if (!starting)
            {
                StartCoroutine("StartGame");
            }

            if (fps < 1 && !otherManager.gameOver)
            {
                gameOver = true;
                otherManager.gameOver = true;
                StartCoroutine("GameOver");
                return;
            }
            else if (fps > 1 && gameOver) {
                StartCoroutine("GameWin");
            }
            timer = Time.fixedTime - gameStartTime - timeConsumed;
            float currentRate = 1 / Mathf.Clamp(fps, 0, 60); //cap actual framerate at 60 but let them have more "health"
            if (timer > currentRate && localPlayer.GetComponent<PlayerMovement>().enabled)
            {
                timeConsumed += currentRate;
                NextFrame(currentRate);
            }
        }
    }

    IEnumerator GameOver()
    {
        endLoseUI.SetActive(true);
        localPlayer.GetComponent<PlayerMovement>().enabled = false;
        yield return new WaitForSeconds(3f);
    }

    IEnumerator GameWin()
    {
        localPlayer.GetComponent<PlayerMovement>().enabled = false;
        yield return new WaitForSeconds(1f);
        endWinUI.SetActive(true);
        winText.text = "Other Player Disconneced \n You are the only Remaining Player";
        yield return new WaitForSeconds(2f);
        winText.text = "You Win!!!";
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    IEnumerator StartGame()
    {
        starting = true;
        pregameUI.SetActive(false);
        localPlayer.GetComponent<PlayerMovement>().enabled = false;

        startCountdown.gameObject.SetActive(true);
        startCountdown.text = "Game Starts in \n 3";
        yield return new WaitForSeconds(1f);
        startCountdown.text = "Game Starts in \n 2";
        yield return new WaitForSeconds(1f);
        startCountdown.text = "Game Starts in \n 1";
        yield return new WaitForSeconds(1f);
        startCountdown.gameObject.SetActive(false);
        localPlayer.GetComponent<PlayerMovement>().enabled = true;
        gameStartTime = Time.fixedTime;
    }

    private void NextFrame(float currentRate)
    {


        updateFiles();

        localPlayer.GetComponent<PlayerMovement>().FPS = currentRate;
        localPlayer.GetComponent<PlayerMovement>().LaggedUpdate();
        otherPlayer.GetComponent<FollowWithOffset>().LaggedUpdate(currentRate);
        for (int i = 0; i < bulletList.Count; i++)
        {
            bulletList[i].MoveBullet(currentRate);
        }
    }

    public void RemoveFile(string file)
    {
        if (files[file] == 0)
            return;
        files[file]--;
        //raise the framerate
        ChangeFPSOne(fpsCostPerFile[file]);
        updateFiles();
    }


    public void AddFile(string file)
    {
        print("gained " + file);
        if (files[file] == 2)
            return;
        files[file]++;
        //lower the framerate
        ChangeFPSOne(-fpsCostPerFile[file]);
        updateFiles();
    }

    private void updateFiles()
    {
        //updates appropriate variables
        playerFPScounter.text = "FPS: " + ((int)fps);
        otherFPScounter.text = "FPS:" + ((int)otherManager.fps);

        FPScounter(files["fps"]);
        ScreenWrapCounter(files["screenWrap"]);
        BouncyCounter(files["bouncyBlock"]);
        IcyCounter(files["icyBlock"]);
        UpdateQueue();
    }
    void UpdateQueue()
    {
        foreach(GameObject eachUIGuy in queuedItemsInUI)
        {
            eachUIGuy.SetActive(false);
        }
        if (abilities.Count > 0)
        {
            UIItems[abilities[0]].SetActive(true);
        }
    }
    private void BouncyCounter(int count)
    {
        if (count == 0 && otherManager.files["bouncyBlock"] != 2)
        {
            bouncyBlockHolder.SetActive(false);
        }
        else if (count == 1)
        {
            bouncyBlockHolder.SetActive(true);
            bouncyBlockHolder.transform.position = bouncyBlockOrigin;
        }
        else if (count == 2)
        {
            bouncyBlockHolder.SetActive(true);
            bouncyBlockHolder.transform.position = bouncyBlockOrigin;
            otherManager.bouncyBlockHolder.SetActive(true);
            otherManager.bouncyBlockHolder.transform.position = bouncyBlockOrigin + blockOffset;
        }
    }
    private void IcyCounter(int count)
    {
        if (count == 0 && otherManager.files["icyBlock"] != 2)
        {
            icyBlockHolder.SetActive(false);
        }
        else if (count == 1)
        {
            icyBlockHolder.SetActive(true);
            icyBlockHolder.transform.position = icyBlockOrigin;
        }
        else if (count == 2)
        {
            icyBlockHolder.SetActive(true);
            icyBlockHolder.transform.position = icyBlockOrigin;
            otherManager.icyBlockHolder.SetActive(true);
            otherManager.icyBlockHolder.transform.position = icyBlockOrigin + blockOffset;
        }
    }

    private void FPScounter(int count)
    {
        //print(otherFPScounter.gameObject.activeInHierarchy);
        if (count == 0)
        {
            playerFPScounter.gameObject.SetActive(false);
            otherFPScounter.gameObject.SetActive(false);
        }
        else if (count == 1)
        {
            playerFPScounter.gameObject.SetActive(true);
            otherFPScounter.gameObject.SetActive(false);
        }
        else if (count == 2)
        {
            playerFPScounter.gameObject.SetActive(true);
            otherFPScounter.gameObject.SetActive(true);
        }
    }


    //WILL HAVE TO MAKE WALLS AT ONE POINT (or player position checker)
    private void ScreenWrapCounter(int count)
    {
        if (count == 0)
        {
            screenWrap.SetActive(false);
            screenWrap.transform.position = screenWrapOrigional;
            walls.SetActive(true);
        }
        else if (count == 1)
        {
            screenWrap.SetActive(true);
            screenWrap.transform.position = screenWrapOrigional;
            walls.SetActive(false);
        }
        else if (count == 2)
        {
            screenWrap.SetActive(true);
            screenWrap.transform.position = screenWrapOrigional + screenWrapOffset;
            walls.SetActive(false);
        }
    }


    public void ChangeFPSOne(float lag)
    {
        fps += lag;
    }
}
