﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FileManager : MonoBehaviour {

    public GameManager gameManager;

    public List<Image> fileList;
    public string playerNumber;
    public bool finished;

    public float amountLeft = 0;

    public Text choicesLeft;
    bool neutral=true;
    private int selected;
	// Use this for initialization
	void Start () {
        selected = 0;
        HighlightText();
        choicesLeft.text = "Choices Left: " + (fileList.Count - amountLeft);
    }

    private void HighlightText() {
        for (int i = 0; i < fileList.Count; i++) {
            if(selected == i)
                fileList[i].color = new Color(.35f, .45f, 1f, 1);
            else
                fileList[i].color = new Color(1f, 1f, 1f, 1);
        }
    }

    private void SelectFile() {
        fileList[selected].color = new Color(.62f, .62f, .62f, 1);
        string newKey = GetKey(fileList[selected].transform.GetChild(0).GetComponent<Text>().text);
        if (newKey == "")
            return;
        gameManager.abilities.Add(newKey);
        fileList.RemoveAt(selected);
        if (selected >= fileList.Count)
            selected = fileList.Count - 1;

        choicesLeft.text = "Choices Left: " + (fileList.Count - amountLeft);


        HighlightText();
    }

    private string GetKey(string text)
    {
        if (text.Contains("Bouncy"))
            return "bouncyBlock";
        if (text.Contains("Icy"))
            return "icyBlock";
        if (text.Contains("Screen"))
            return "screenWrap";
        if (text.Contains("FPS"))
            return "fps";
        return "";
    }

    private void MoveUp() {
        selected--;
        if (selected < 0)
            selected = fileList.Count - 1;
        HighlightText();
    }

    private void MoveDown()
    {
        selected++;
        if (selected == fileList.Count)
            selected = 0;
        HighlightText();
    }

    void Update() {

        if (fileList.Count > amountLeft)
        {
            if (Input.GetAxis("Vertical" + playerNumber) > 0 && Input.GetButtonDown("Vertical" + playerNumber) ||(
                Input.GetAxis("VerticalJoy" + playerNumber) > 0 && neutral))
            {
                neutral = false;
                MoveUp();
            }
            else if (Input.GetAxis("Vertical" + playerNumber) < 0 && Input.GetButtonDown("Vertical" + playerNumber) ||(
                Input.GetAxis("VerticalJoy" + playerNumber) < 0 && neutral ))
            {
                MoveDown();
                neutral = false;
            }
            else
            {
                //stick in neutral
                if (Input.GetAxis("VerticalJoy" + playerNumber) == 0)
                {
                    neutral = true;
                }
            }
            if (Input.GetButtonDown("NormalGun" + playerNumber) || Input.GetButtonDown("Jump"+playerNumber) || Input.GetButtonDown("LagGun" + playerNumber))
            {
                SelectFile();
            }
        }
        else
            finished = true;
    }
}
