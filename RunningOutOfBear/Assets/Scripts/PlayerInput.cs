﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
    public string playerNumber;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool NormalFire()
    {
        return Input.GetButton("NormalGun" + playerNumber);
    }
    public bool LagFire()
    {
        return Input.GetButton("LagGun" + playerNumber);
    }
    public float HInput()
    {
        return AxisInput("Horizontal");
    }
    public float VInput()
    {
        return AxisInput("Vertical");
    }
    public bool Jump()
    {
        return Input.GetButton("Jump" + playerNumber);
    }
    float AxisInput(string axis)
    {
        float keyboard = Input.GetAxis(axis + playerNumber);
        float joystick = Input.GetAxis(axis+"Joy" + playerNumber);
        if (Mathf.Abs(keyboard) > Mathf.Abs(joystick))
        {
            return keyboard;
        }
        else
        {
            return joystick;
        }
    }
}
