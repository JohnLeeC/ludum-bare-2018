﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallClamp : MonoBehaviour {
    public bool horizontal, goodDir;
    public GameObject player;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ClampPlayer();

    }

    private void ClampPlayer() {
        if (horizontal)
        {
            if (goodDir && player.transform.position.x > transform.position.x - transform.lossyScale.x / 2 - player.transform.lossyScale.x / 2)
            {
                player.transform.position = new Vector3(transform.position.x - transform.lossyScale.x / 2 - player.transform.lossyScale.x / 2, player.transform.position.y, player.transform.position.z);
            }
            else if (!goodDir && player.transform.position.x < transform.position.x + transform.lossyScale.x / 2 + player.transform.lossyScale.x / 2)
            {
                player.transform.position = new Vector3(transform.position.x + transform.lossyScale.x / 2 + player.transform.lossyScale.x / 2, player.transform.position.y, player.transform.position.z);
            }
        }
        else
        {
            if (goodDir && player.transform.position.y > transform.position.y - transform.lossyScale.y / 2 - player.transform.lossyScale.y / 2)
            {
                player.transform.position = new Vector3(player.transform.position.x, transform.position.y - transform.lossyScale.y/ 2 - player.transform.lossyScale.y / 2, player.transform.position.z);
            }
            else if (!goodDir && player.transform.position.y < transform.position.y + (transform.lossyScale.y / 2) + player.transform.lossyScale.y / 2)
            {
                player.GetComponent<PlayerMovement>().grounded = true;
                player.transform.position = new Vector3(player.transform.position.x, transform.position.y + (transform.lossyScale.y/2) + player.transform.lossyScale.y/2, player.transform.position.z);
            }
        }


    }
}
