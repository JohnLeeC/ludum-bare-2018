﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LagGun : NormalGun
{
    [SerializeField] Image bar;

    public bool isFollower;
    
    //public string itemToSend;
    public override BulletMovement Fire(Vector2 dir, BulletMovement otherBullet = null)
    {
        
        BulletMovement bul = base.Fire(dir, otherBullet);
        LagBulletMovement isThisALagBullet = bul.gameObject.GetComponent<LagBulletMovement>();


        if (isThisALagBullet && manager.abilities.Count>0)
        {
            
            if (!isFollower)
            {

                isThisALagBullet.itemToSend = manager.abilities[0];
                print(manager.abilities[0] + " removed from " + manager);
                manager.RemoveFile(manager.abilities[0]);
                manager.abilities.RemoveAt(0);
            }
            else
            {

                isThisALagBullet.itemToSend = manager.otherManager.abilities[0];
            }
        }
        return bul; 


    }

    public override void LaggedUpdate(float fps)
    {
        FPS = fps;
        SetDir();
        if (InputBool() && cooldownTimer <= 0 && manager.abilities.Count > 0)
        {
            Fire(direction);
        }
        CooldownCountdown();
        if(bar)
            bar.fillAmount = 1- (cooldownTimer / cooldown);
    }

    protected override bool InputBool()
    {
        return input.LagFire();
    }

}
