﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWrap : MonoBehaviour {

    public ScreenWrap other;
    public Transform spawner;
    public GameObject player;
    public bool horizontal, goodDir;
    private List<GameObject> collided = new List<GameObject>();


	void Update () {
        MoveObejct();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Vector3 objDir;
        if (collision.tag == "bullet")
        {
            objDir = collision.GetComponent<BulletMovement>().dir;
        }
        else if (collision.tag == "Player")
        {
            objDir = collision.GetComponent<PlayerMovement>().velocity.normalized;
        }
        else if (collision.tag == "corpse") {
            objDir = collision.GetComponent<Rigidbody2D>().velocity.normalized;
        }
        else
            return;


        if (horizontal)
        {
            if (goodDir && objDir.x > 0)
            {
                collided.Add(collision.gameObject);
            }
            else if (!goodDir && objDir.x < 0)
            {

                collided.Add(collision.gameObject);
            }
        }
        else {
            if (goodDir && objDir.y > 0)
                collided.Add(collision.gameObject);
            else if (!goodDir && objDir.y < 0) 
             collided.Add(collision.gameObject);
        }
    }

    //make public if wanna include this in game loop
    private void MoveObejct() {
        for (int i = 0; i < collided.Count; i++)
        {
            if (collided[i])
            {
                if (horizontal)
                {
                    collided[i].transform.position = new Vector3(other.spawner.position.x, collided[i].transform.position.y, collided[i].transform.position.z);
                }
                else
                {
                    collided[i].transform.position = new Vector3(collided[i].transform.position.x, other.spawner.position.y, collided[i].transform.position.z);

                }
            }
        }

        if (horizontal)
        {
            if (goodDir && player.transform.position.x > transform.position.x)
                player.transform.position = new Vector3(other.spawner.position.x, player.transform.position.y, player.transform.position.z);
            else if (!goodDir && player.transform.position.x < transform.position.x)
                player.transform.position = new Vector3(other.spawner.position.x, player.transform.position.y, player.transform.position.z);
        }
        else
        {
            if (goodDir && player.transform.position.y > transform.position.y)
                player.transform.position = new Vector3(player.transform.position.x, other.spawner.position.y, player.transform.position.z);
            else if (!goodDir && player.transform.position.y < transform.position.y)
                player.transform.position = new Vector3(player.transform.position.x, other.spawner.position.y, player.transform.position.z);
        }
        collided.Clear();

    }
}
