﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour {

    public GameManager gameManager;
    BulletMovement twinBullet;
    public Vector3 dir;
    public float moveSpeed;
    public float age = 0;
    public string creator;
    bool destroyed = false;
    [SerializeField] float bulletFPSCost;
    public void Start()
    {
    }

    public void BulletStart( Vector3 newDir, GameManager managerRef,string player, BulletMovement otherBullet)
    {
        creator = player;
        gameManager = managerRef;
        dir = newDir;
        gameManager.bulletList.Add(gameObject.GetComponent<BulletMovement>());
        twinBullet = otherBullet;
        gameManager.ChangeFPSOne(-bulletFPSCost);
    }

    public void MoveBullet(float fps) {
        gameObject.transform.position += moveSpeed * dir * fps;
        age += 1*fps;

    }

    public void DestroyBullet() {
        destroyed = true;
        if(!twinBullet.destroyed)
            twinBullet.DestroyBullet();
        gameManager.bulletList.Remove(gameObject.GetComponent<BulletMovement>());

        gameManager.ChangeFPSOne(bulletFPSCost);
        Destroy(gameObject);
    }
}
