﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PressAnything : MonoBehaviour {
    [SerializeField] string sceneToLoad;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if (Input.anyKey|| Input.GetButtonDown("NormalGun1") || Input.GetButtonDown("NormalGun2") || Input.GetButtonDown("Jump1") || Input.GetButtonDown("Jump2"))
        {
            SceneManager.LoadScene(sceneToLoad);
        }

	}
}
