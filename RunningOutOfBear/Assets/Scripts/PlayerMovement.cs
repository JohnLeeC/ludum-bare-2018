﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    PlayerInput input;
    PlayerAnimator animScript;
    [Header("Movement Vars")]
    [SerializeField]
    float hSpeed, jumpStrength, maxFallSpeed, gravity, hDeAccel, threshhold=0,hopTime;
    //live physics bois
    public Vector3 velocity;
    public float bounceStrength, iceSpeedDecay;
    [Header("Collision Vars")]

    NormalGun[] guns;

    [SerializeField]
    Transform leftCast, rightCast, floorCast1, floorCast2, ceilCast;
    [SerializeField] public bool grounded = false;
    [SerializeField] Vector2 floorCheckSize;
    [SerializeField] public float FPS;
    float hopTimer = 0;

    RaycastHit2D leftRay, rightRay, ceilRay;
	// Use this for initialization
	void Awake () {
        input = GetComponent<PlayerInput>();
        animScript = GetComponent<PlayerAnimator>();
        guns = GetComponents<NormalGun>();
    }
    private void Update()
    {
        //FPS = Time.deltaTime;
      //  LaggedUpdate();
    }
    public void LaggedUpdate()
    {
        ApplyInput();
        UpdateRays();
        ApplyMovement();
        ApplyCollision();
        animScript.Animate();

        foreach (NormalGun gun in guns)
        {
            gun.LaggedUpdate(FPS);
        }
    }

    
    /// <summary>
    /// converts input into velocity
    /// </summary>
    void ApplyInput()
    {
        velocity = new Vector3(HorizontalSpeed(), VerticalSpeed());
    }
    /// <summary>
    /// calculate hspeed, might get more finessed later
    /// </summary>
    float HorizontalSpeed()
    {
        RaycastHit2D ray1 = Physics2D.Linecast(transform.position, floorCast1.position);
        RaycastHit2D ray2 = Physics2D.Linecast(transform.position, floorCast2.position);


        if ((ray1 && ray1.collider.tag == "IcyGround") || (ray2 && ray2.collider.tag == "IcyGround"))
        {
            float lastInput = velocity.x / hSpeed;


            if (lastInput <= input.HInput() && lastInput < 0 && input.HInput() != -1 && input.HInput() <= 0)
            {
                return (lastInput * hSpeed + iceSpeedDecay * FPS);
            }
            else if (lastInput >= input.HInput() && lastInput > 0 && input.HInput() != 1 && input.HInput() >= 0)
            {

                return (lastInput * hSpeed - iceSpeedDecay * FPS);
            }
        }
        return input.HInput() * hSpeed;
    }
    float VerticalSpeed()
    {   
        if (grounded)
        {
            if (input.VInput() > threshhold || input.Jump())
            {
                grounded = false;
                hopTimer = hopTime;
                return velocity.y + jumpStrength-jumpStrength*2*FPS;
            }
            else
            {
                return 0;
            }
            
        }
        else
        {
            if ((input.VInput() > threshhold || input.Jump()) && hopTimer>0)
            {
                hopTimer -= FPS;
                return (velocity.y);
            }
            else if (input.VInput() < -threshhold)
            {
                //fastfall
                return (-maxFallSpeed);
            }
            else
            {
                hopTimer = 0;
                return (Mathf.Clamp(velocity.y - (gravity*FPS), -maxFallSpeed, 100));
            }
        }
    }
    void ApplyCollision()
    {
        if (leftRay && velocity.x<-threshhold){
            velocity.x = 0;
            //wall it
            transform.position = new Vector2(leftRay.point.x + .5f, transform.position.y);
        }
        else if(rightRay && velocity.x > threshhold)
        {
            velocity.x = 0;
            //wall it
            transform.position = new Vector2(rightRay.point.x - .5f, transform.position.y);
        }
        //vertical
        if (ceilRay && velocity.y > threshhold)
        {
            velocity.y = 0;

            //ceil it
            transform.position = new Vector2(transform.position.x, ceilRay.point.y - .5f);

            //so you cant stick to the ceilling

            hopTimer = 0;
        }
        else
        {
            if (FloorCheck() && velocity.y < -threshhold)
            {
                velocity.y = 0;
                grounded = true;

            }
            if( grounded && !FloorCheck() )
            {
                grounded = false;
            }
        }
    }
    void UpdateRays()
    {
        ceilRay = RaycastPoint(ceilCast.position);
        leftRay = RaycastPoint(leftCast.position);
        rightRay= RaycastPoint(rightCast.position);
    }
    RaycastHit2D RaycastPoint(Vector3 point)
    {
        RaycastHit2D ray = Physics2D.Linecast(transform.position, point);
        return ray;
    }
    
    bool FloorCheck()
    {
        //check the whole length of ye for floor checks
        RaycastHit2D ray1 = Physics2D.Linecast(transform.position, floorCast1.position);
        RaycastHit2D ray2 = Physics2D.Linecast(transform.position, floorCast2.position);
        if (ray1)
        {            //snap to the floor
            transform.position =  new Vector2(transform.position.x, ray1.point.y+.5f);

            if (ray1.collider.tag == ("BouncyGround"))
            {
                velocity.y = bounceStrength;
                hopTimer = hopTime;
            }
            return true;
        }
        else if(ray2)
        {
            //snap to the floor
            transform.position = new Vector2(transform.position.x, ray2.point.y + .5f);
            //print(ray1 )
            if (ray2.collider.tag == ("BouncyGround"))
            {
                velocity.y = bounceStrength;
                hopTimer = hopTime;
            }
            return true;
        }
        else return false;
    }



	void ApplyMovement()
    {
        transform.position += velocity * FPS ;
    }

}
