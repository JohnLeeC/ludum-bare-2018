﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour {


    Animator animComp;
    SpriteRenderer spriteComp;
    bool moving;
    PlayerInput input;
    // Use this for initialization
    void Awake() {

        animComp = GetComponent<Animator>();
        spriteComp = GetComponent<SpriteRenderer>();
        input = GetComponent<PlayerInput>();
    }

    public void Animate()
    {
        float hInput = input.HInput();
        if (hInput > 0)
        {
            spriteComp.flipX = false;
            if (!moving)
            {
                moving = true;
                animComp.Play("walk");
            }
        }
        else if (hInput < 0)
        {
            spriteComp.flipX = true;
            if (!moving)
            {
                moving = true;
                animComp.Play("walk");
            }
        }
        else
        {
            if (moving)
            {
                moving = false;
                animComp.Play("idle");
            }
        }
    }
}
