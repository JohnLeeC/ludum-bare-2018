﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowWithOffset : MonoBehaviour {
    Vector3 offset;
    Transform target;
    PlayerAnimator animScript;
	// Use this for initialization
	public void StartFollow (Transform newTarget) {
        target = newTarget;
        offset = target.position-transform.position;
        animScript = GetComponent<PlayerAnimator>();
    }
	
	// Update is called once per "frame"
	public void LaggedUpdate (float FPS) {
        transform.position = target.position - offset;
        animScript.Animate();
    }
    
}
