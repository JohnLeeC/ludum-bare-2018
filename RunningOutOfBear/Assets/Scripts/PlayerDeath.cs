﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    PlayerInput input;
    [SerializeField] PlayerDeath twin;
    Vector3 spawnPosition;
    [SerializeField] GameObject corpsePrefab;
    [SerializeField] GameManager manager;
    SpriteRenderer spriteRef;
    float ageOfHazard = .5f;
    float spawnInvince = 30;
    bool died = false;
    [SerializeField] float corpseLagCost;
    private void Awake()
    {
        spawnPosition = transform.position;
        input = GetComponent<PlayerInput>();
        spriteRef = GetComponent<SpriteRenderer>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {


        if (collision.tag == "bullet")
        {
            BulletMovement bul = collision.GetComponent<BulletMovement>();
            //if (bul.gameObject.GetComponent<LagBulletMovement>() != null)
            //    print(bul.gameObject.GetComponent<LagBulletMovement>().itemToSend + " fucking should spawned on screen of " + manager);


            if (bul.creator != input.playerNumber) //if its the other players bullet
            {
                Death(bul);
            }
            else if (bul.age > ageOfHazard)
            { //or an old bullet of yourss
                Death(bul);
            }
        }
    }
    private void Death(BulletMovement bul)
    {
        if (!died)
        {



            if (corpsePrefab)
            {
                Instantiate(corpsePrefab, transform.position, Quaternion.identity).transform.parent = manager.corpseHolder;
                if (bul.gameObject.GetComponent<LagBulletMovement>() != null)
                {
                    print(bul.gameObject.GetComponent<LagBulletMovement>().itemToSend + " spawned on screen of " + manager);
                    manager.AddFile(bul.gameObject.GetComponent<LagBulletMovement>().itemToSend);
                }
                manager.ChangeFPSOne(-corpseLagCost);

                died = true;
                bul.DestroyBullet();
                twin.Death(bul);


                transform.position = spawnPosition;
                //one second of invincibility
                StartCoroutine(Flicker());

            }
        }
    }
    IEnumerator Flicker()
    {
        for (int i = 0; i < spawnInvince; i++)
        {
            spriteRef.enabled = false;
            yield return new WaitForSeconds(.05f);
            spriteRef.enabled = true;

            yield return new WaitForSeconds(.05f);
        }
        died = false;
    }
}
