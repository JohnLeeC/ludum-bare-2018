﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LagRigidBody : MonoBehaviour {
    Rigidbody2D rigidComp;
	// Use this for initialization
	void Start () {
		
	}
	
	public void LaggedUpdate()
    {
        StartCoroutine(oneFrameOfPhysics());

    }
    IEnumerator oneFrameOfPhysics()
    {
        rigidComp.simulated = true;
        yield return new WaitForEndOfFrame();
        rigidComp.simulated = false;
    }
}
