﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalGun : MonoBehaviour
{

    protected Vector3 direction = Vector3.right;
    protected PlayerInput input;
    [SerializeField]protected GameManager manager;
    [SerializeField] protected float cooldown;
    [SerializeField] GameObject bulletType;
    [SerializeField] Transform leftPoint, rightPoint;
    [SerializeField] NormalGun follower;
    Transform spawnPoint;
    protected float cooldownTimer = 0;
    protected float FPS;
    // Use this for initialization
    void Awake()
    {

        input = GetComponent<PlayerInput>();
        spawnPoint = rightPoint;
    }
    private void Update()
    {
    }
    public virtual void LaggedUpdate(float fps)
    {
        FPS = fps;
        SetDir();
        if ( InputBool() && cooldownTimer <= 0)
        {
            Fire(direction);
        }
        CooldownCountdown();
    }
    protected virtual bool InputBool()
    {
        return input.NormalFire();
    }
    protected void SetDir()
    {
        if (input.HInput() < 0)
        {
            direction = -Vector2.right;
        }
        else if (input.HInput() > 0)
        {
            direction = Vector2.right;
        }
    }
    public virtual BulletMovement Fire(Vector2 dir,BulletMovement otherBullet = null )
    {

        cooldownTimer = cooldown;

        GameObject newBullet = Instantiate(bulletType, spawnPoint.position, Quaternion.identity);
        
        newBullet.transform.position = spawnPoint.position;
        newBullet.transform.parent = manager.bulletHolder;
        BulletMovement newBulletScript = newBullet.GetComponent<BulletMovement>();
        if (otherBullet)
        {
            newBulletScript.BulletStart(dir, manager, input.playerNumber, otherBullet);

        }
        else
        {
            newBulletScript.BulletStart(dir, manager, input.playerNumber, follower.Fire(dir,newBulletScript));

        }
        return newBulletScript;
    }

    protected void CooldownCountdown()
    {
        if (cooldownTimer > 0)
        {
            cooldownTimer -= FPS;
        }
    }
}
